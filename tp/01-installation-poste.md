## Installation de votre environnement.
+ Installation du controller ansible depuis la vm **ansible_controller (192.168.56.200)**:  
```shell
sudo apt update
sudo apt install ansible
```
+ Générer les clefs ssh:  
```shell
ssh-keygen -t ecdsa
```

+ Mettre à jour le ficher **/etc/hosts**:  
```shell
192.168.56.201 node1  
192.168.56.202 node2  
192.168.56.203 node3  
192.168.56.204 node4  
```
+ Les copier ssh-copy: 
```shell 
ssh-copy-id -i ~/.ssh/id_ecdsa node1
ssh-copy-id -i ~/.ssh/id_ecdsa node2
ssh-copy-id -i ~/.ssh/id_ecdsa node3
ssh-copy-id -i ~/.ssh/id_ecdsa node4
```
+ Mise en place d'un agent ssh:
```shell 
eval `ssh-agent`
ssh-add
```

## Configuration de votre environnement.
+ Mettre à jour votre configuration ansible:
```shell 
deprecation_warnings = False
host_key_checking = False
```
+ Définition de votre inventaire:
Renommer le fichier /etc/ansible/hosts en /etc/ansible/hosts.origin.  
Créer un nouveau fichier /etc/ansible/hosts avec:  
```yml 
[ubuntu_servers]
node1
node2

[centos_servers]
node3
node4
```
Vérifier le bon fonctionnement avec le module ping.


