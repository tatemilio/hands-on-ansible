## Exercice 1: 
Créer le répertoire lab_roles. Positionnez-vous sur ce répertoire.  
Créer le role **utilisateur** via la commande suivante:  
ansible-galaxy init roles/utilisateur

L'objectif de ce role est de:   
1- Creer un groupe en utilisant les paramètres fournis:  
**grp_name**: pour le nom du groupe  
**grp_gid**: pour le gid du groupe  
⚠️ Si le gid n'est pas défini, utiliser la valeur par défaut : 1414

2- Créer un utilisateur en utilisants les paramètres fournis: 
**user_name** : nom de l'utilisateur
**user_uid** : uid de l'utilisateur
Pour le group utiliser la variable **grp_name**  
concernant la home : /home/'le nom de l'utilisateur'

Lorsque le role est terminé, veuillez écrire un playbook **create_utilisateur** qui :  
Etape 1:
Appel le role pour créer l'utilisateur suivant:  
🧑 alice avec uid=1010, groupe team, répertoire de connexion /home/alice  
⚠️ Ne pas spécifier le gid, il devrat prendre la valeur par défaut **1414**  
**import_role**

Etape 2:  
Appel du role dans une boucle pour créer les utilisateurs suivants:  
🧑 david avec uid=1020, groupe cgi (gid 1415), répertoire de connexion /home/david  
🧑 marcel avec uid=1021, groupe cgi (gid 1415), répertoire de connexion /home/marcel  
**include_role**


