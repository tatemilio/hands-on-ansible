## Inventaire
Définir un fichier d'inventaire avec 3 groupes web,bdd,masters:  
1- Définir les machines node1 à node5 dans le groupe **web**.    
2- Définir les machines node6 à node10 dans le groupe **bdd**.  
3- Définir les machines node1 et node6 dans le groupe **masters**.  
  
Directement dans le fichier inventaire définir:  
1- Pour le group bdd, un paramètre user dont la valeur est user  
2- Pour la machine node6, un paramètre user dont la valeur est user_node6
Vérifier le résultat par la commande: 
``` yaml
ansible-inventory -i <chemin de votre fichier> --graph --vars
```
  
Depuis host_vars et group_vars:  
1- Définir les paramètres suivants pour le group web:    
user=user_web  
pwd1=pwd1  
pwd2=pwd2  
Vérifier le résultat par la commande: Précédence ...
``` yaml
ansible-inventory -i <chemin de votre fichier> --graph --vars
```
    
2- Définir les paramètres suivants pour le node6:  
user:user_web_node6  
pwd:pwd_node6  

Vérifier le résultat par la commande: Précédence ...
``` yaml
ansible-inventory -i <chemin de votre fichier> --graph --vars
``` 

## Vault
Créer 1 fichier de vault contenant votre mot de passe de chiffrement  
1- Chiffrer le fichier de paramètres du node6

``` yaml
ansible-vault encrypt ...
https://docs.ansible.com/ansible/2.9/user_guide/vault.html#providing-vault-passwords
``` 

Vérifier le résultat par la commande: Précédence ...
``` yaml
ansible-inventory -i <chemin de votre fichier> --graph --vars --???
``` 

2- Identity List
Créer 2 fichiers contenant deux mots de passe de chiffrement (secret_vault1, secret_vault2).  
Exporter la variable d'environnement ANSIBLE_VAULT_IDENTITY_LIST contenant les id et les fichiers de chiffrement  
Définir les paramètres suivants pour le group **masters**  
var1 : Encrypter la chaine de caractère string_vault1 avec le fichier secret_vault1   
var2 : Encrypter la chaine de caractère string_vault2 avec le fichier secret_vault2   
``` yaml
echo -n "xxx" | ansible-vault encrypt_string --encrypt-vault-id=xxx  
``` 
Vérifier le résultat par la commande
``` yaml
ansible-inventory -i <chemin de votre fichier> --graph --vars --???
``` 



