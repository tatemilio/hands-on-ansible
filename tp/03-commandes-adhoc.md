## Commandes Adhoc
+ Exécution d'une commande: 
Exécutez la commande suivante sur le groupe **centos_server**: free -h

+ Exécution de plusieurs commandes: 
Exécutez la commande suivante sur le groupe **centos_server**: date; sleep 5; date

+ Copie de fichiers : 
Copier le fichier /etc/hosts de votre serveur Ansible vers un fichier /tmp/MACHINES des machines 
de votre groupe **centos_servers**.  
Le fichier /tmp/MACHINES aura les caractéristiques suivantes:  
permissions: 444  
propriétaire: vagrant   
groupe propriétaire: vagrant  
Vérifier les permissions, du fichier /tmp/MACHINES.  
Reexécuter la commande de copie et constater la différence des messages  

+ Suppression de fichiers:
Pour le groupe **centos_servers** : supprimer le fichier /tmp/MACHINES  
⚠️ Ne pas supprimer /etc/hosts !!!!

+ Installation et suppression de package:
Pour le groupe **centos_servers**  
Vérifier si une version du package nmap est disponible.  
Installer le package nmap dans sa dernière version.  
Supprimer le package nmap.

+ Création d'un utilisateur:
Pour toutes les machines créer l'utilisateur **cgi** avec les caractéristiques suivantes:  
Groupe Primaire: cgi (gid 1212)
Répertoire de connection: /home/user
Pas de mot de passe

+ Redémarrage d'un service
Pour le server **node4** redémarrer le service **crond**

+ Ouverture d'un service sur le firewall.  
Toujours pour le server **node4**:  
Ajouter une règle **permanente** pour laisser passer le service **http** à travers le firewall.  
> 💡 Consulter la documentation du module **firewalld** pour connaitre les paramètres du module.  

Recharger la configuration du firewall pour une application immédiate.    
Pour cela :   
1- Démarrer le service 'firewalld' (utiliser le module **systemd**)  
2- Ajout de la règle en utilisant le module ansible **firewalld**    
3- Vérification sur le node4    
```shell
sudo su # Pour passer root  
cat /etc/firewalld/zones/public.xml
```  

+ Le module Scripts  
Créer le script suivant dans votre répertoire courant:  
```shell
#!/bin/bash
echo "Début du traitement"
touch /tmp/fichier_monscript
echo "Fin du traitement"
``` 
Exécuter ce script sur la cible **node3**.  
Vérifier la présence du fichier crée sur la cible par une commande Adhoc.    

+ Le module fetch
Récupérer une copie des fichiers /etc/hosts de toutes les cibles sur votre serveur Ansible.    
Ces fichiers seront récupérés dans un répertoire resultat.  
    
⚠️ ne pas utiliser le module **copy**  

  
