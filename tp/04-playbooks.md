## Exercice 1: Installation Apache
Créer le répertoire apache_install. Positionnez-vous sur ce répertoire.  
Copier les fichiers httpd.conf et index_install.html  
Créer le playbook apache_install.yml qui doit réaliser les opérations suivantes sur les postes du
groupe **centos_servers**:  

- Installation du package Apache (nom du package: **httpd**).  
- Copie du fichier de configuration httpd.conf sous le répertoire /etc/httpd/conf.  
- Installation de la page de garde du site:  
copier le fichier index_install.html sous /var/www/html/index.html.  
  
- Redémarrer le service apache.  

Vérifier la syntaxe du fichier par: --syntax-check  
Vérifier le fonctionnement par: --check  

Exécuter le playbook.
Vérifier avec votre navigateur: http://192.168.56.203 http://192.168.56.204  

Question subsidiaire, à la fin du playbook ajouter les actions suivantes :  

- Ouvrir le service http sur le pare-feu.  
- Recharger la configuration du pare-feu pour une application immédiate.  

## Exercice 2: Les variables

Créer le répertoire variables. Positionnez-vous sur ce répertoire.  

Etape 1 :  
Le playbook variables.yml doit afficher le contenu de la variable compte_bdd.  

Créer deux répertoires group_vars et host_vars au même niveau que votre playbook.  
Vous devez exploiter ces répertoires au mieux pour que la variable compte_bdd ait la valeur **none**  
pour tous les postes, sauf pour les machines du groupe centos_servers où elle correspond à **operateur**.  
Une exception concerne la machine **node4** où la valeur doit être **administrateur**.  
  
Veuillez suivre les étapes suivantes:  
  
Faites que la variable ait **none** pour tous les postes.  
Faites votre playbook et tester le.    
  
Faites que la variable ait **operateur** pour les postes du groupe centos_servers.  
Vérifier par une exécution de votre playbook.  
  
Faites que la valeur ait **administrateur** pour le poste note4.  
Vérifier par une exécution de votre playbook.  
  

Etape 2 :
Exceptionnellement, par exemple pour un test spécifique, vous devez exécuter votre playbook en
ligne de commandes avec la variable positionnée à **TEST**.  


## Exercice 3: Les templates
Créer un répertoire apache_template. Positionnez-vous sur ce répertoire.  
Copier les fichiers httpd.conf et index_template.j2.  
  
Etape 1 :  
Créer le playbook apache_template.yml pour le groupe **centos_servers**.  
  
Il doit réaliser les mêmes opérations que l'exercice 1.  
La différence concerne la page de garde (index.html) qui cette fois est un template.  
Les étapes:
- Créer le playbook qui utilisera le template pour la page de garde du site: /var/www/html/index.html
- Vérifier la syntaxe du fichier par: --syntax-check
- Exécuter le playbook.  
- Vérifier par votre navigateur: http://192.168.56.203 http://192.168.56.204  
  

## Exercice 4: Debug et Register
Créer un répertoire debug_register. Positionnez-vous sur ce répertoire.  
Créer le playbook affichage.yml pour toutes les machines.  
Le playbook doit récupérer le résultat de la commande suivante: **free -h**  
Le playbook doit utiliser le module debug pour:  
afficher stdout,  
afficher stdout_lines,  
afficher stdout_lines[1],  
afficher un message indiquant:  
- la commande utilisée,  
- l'option de la commande utilisée,  
- le code de retour.  

## Exercice 5 : Les handlers
Créer le répertoire apache_handler. Positionnez-vous sur ce répertoire.  
On reprend l'exercice Apache (exercice 1). 
Copiez les fichiers depuis le répertoire apache_install.  

Le playbook à créer est apache_handler.yml. Il concerne les machines du groupe centos_servers.  
  
On décide que:  
• le service Apache ne doit redémarrer que si le fichier de configuration httpd.conf ou si la page web a changé.  

• le firewall doit être rechargé si sa configuration a été modifié ou si le fichier de configuration httpd.conf a changé.  

Pour tester :  
  
Exécuter le playbook selon l'ordre ci-dessous et observer ce qui est installé, modifié et redémarré.  
• 1ère exécution.  
• 2ème exécution.  

• Modifier le fichier index_template.html (ajouter un commentaire).  
3ème exécution.  

• Modifier le fichier httpd.conf (ajouter un commentaire).  
Arrêter le pare-feu sur la machine centos1 avec la commande suivante :  
ansible node4 -m firewalld -a 'service=http permanent=yes state=disabled' -b 
4ème exécution.

• Modifier les deux fichiers (ajouter un commentaire).  
Arrêter le pare-feu sur la machine node4 avec la commande suivante :  
ansible node4 -m firewalld -a 'service=http permanent=yes state=disabled' -b
5ème exécution.

• Supprimer un fichier sur **node3**.  
6ème exécution.  

### Exercice 6 : Les boucles
Créer un répertoire boucles. Positionnez-vous sur ce répertoire.  
Créer le playbook boucle.yml pour toutes les machines.  
Créer les comptes suivants :  
david avec uid=1101, groupe team (gid 1100), répertoire de connexion /home/david  
alice avec uid=1102, groupe team (gid 1100), répertoire de connexion /home/alice  
tatiana avec uid=1103, groupe team (gid 1100), répertoire de connexion /home/tatiana  


Utiliser une variable pour le **gid**.  
Utiliser un tableau nommé **equipe** pour la liste des comptes constitué de :  
nom utilisateur, uid, nom du groupe utilisateur  

### Exercice 7: La condition when
Créer un répertoire condition_when. Positionnez-vous sur ce répertoire.  
Le playbook à créer est cron_when.yml. 
Il concerne toutes les machines.   
Le playbook doit redémarrer le service cron sur les centos et les ubuntu :
pour une **CentOS**, le service est **crond**,  
pour une **Ubuntu**, le service est **cron**  
Consulter la variable ansible_distribution.  

Optionnellement, vous pouvez utiliser le filtre lower ou upper.  

### Exercice 8: Ansible Vault
Créer un répertoire pour cet exercice ( ansible_vault ).  
Réaliser un playbook qui traitera toutes les cibles et qui affichera :  
L application **Nom_Application** a pour mot de passe **Mot_de_Passe_Application**  
La base de donnees **Nom_Base_Donnees** a pour mot de passe **Mot_de_Passe_BDD**  
  
Les variables **Nom_Application** et **Nom_Base_Donnees** sont des variables en claires.  
Les variables des mots de passes sont cryptées.  
Ces variables sont identiques pour les machines du groupe **centos_servers**.   

Elles ne sont définies que pour ce groupe de machines.  
   
Exemple:   
Nom_Application = web Mot_de_Passe_Application = secretpassword1!  
Nom_Base_Donnees = bdd Mot_de_Passe_BDD = secretbdd1!  

Le mot de passe de vault sera stocké dans un fichier localisé dans un répertoire spécifique.   
On devra utiliser les commandes **sans spécifier le fichier du mot de passe vault**.  

Question subsidiaire :  
Au préalable, s‘assurer que les variables Nom_Application et Nom_Base_Donnees sont définies.   
Si elles ne le sont pas, arrêter l‘exécution avec un message d‘erreur spécifique.  

